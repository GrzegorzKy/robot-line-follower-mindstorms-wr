#!/usr/bin/python

from time import *
import sys, argparse, time
from ev3dev import*

#######################USTAWIENIA

lmotor = large_motor(OUTPUT_B)
rmotor = large_motor(OUTPUT_C)
mmotor = medium_motor(OUTPUT_D)
irda = infrared_sensor()
kolor = color_sensor()
swiatlo = light_sensor()      
start = touch_sensor()

assert lmotor.connected
assert rmotor.connected
assert kolor.connected
assert swiatlo.connected
assert irda.connected
assert start.connected
assert mmotor.connected

lmotor.speed_regulation_enabled = 'on'
rmotor.speed_regulation_enabled = 'on'
kolor.mode = "COL-REFLECT"
swiatlo.mode = 'REFLECT'
mmotor.set(speed_regulation_enabled='on', stop_command='hold')
mmotor.set(speed_sp=200)

#######################FUNKCJE

def obroc_sonar(angle) :
	mmotor.run_to_rel_pos(position_sp = angle)

def motorsStaph():


	lmotor.stop()
	rmotor.stop()
def czyPrzeszkoda():
	if irda.value() < 10 :
		return True
	else:
		return False

def obrocRobota(value):
	lmotor.run_to_rel_pos(position_sp = value, speed_sp = 100)
	rmotor.run_to_rel_pos(position_sp = -value, speed_sp = 100)
		
	while 'running' in lmotor.state or 'running' in rmotor.state :
		sleep(0.1)

def ominPrzeszkode(kierunek):
	motorsStaph()
	if kierunek == "lewo":
		obrocRobota(130)
		obroc_sonar(-90)
		for k in range(0,1050):
			lmotor.run_forever(speed_sp = 100)
			rmotor.run_forever(speed_sp = 100)
		motorsStaph()
		sleep(0.1)	
		obrocRobota(-130)
		sleep(0.2)
		if czyPrzeszkoda() == False:
			while czyPrzeszkoda() == False:
				lmotor.run_forever(speed_sp = 200)
				rmotor.run_forever(speed_sp = 200)
			while czyPrzeszkoda() == True:
				lmotor.run_forever(speed_sp = 200)
				rmotor.run_forever(speed_sp = 200)
		else:
			while czyPrzeszkoda() == True:
				lmotor.run_forever(speed_sp = 200)
				rmotor.run_forever(speed_sp = 200)
		
		for k in range(0,1500):
			lmotor.run_forever(speed_sp = 100)
			rmotor.run_forever(speed_sp = 100)		
		motorsStaph()
		sleep(0.1)
		obrocRobota(-130)
		obroc_sonar(90)
		sleep(0.2)
		obroc_sonar(60)
		for i in range(0,950):
			lmotor.run_forever(speed_sp = 100)
			rmotor.run_forever(speed_sp = 100)
		obrocRobota(140)	
	elif kierunek == "prawo":
		print ""
		
def czyKatProsty(kolor,swiatlo):
	if kolor < 12 and swiatlo > 450:
		return "wLewo"
	elif swiatlo < 68 and kolor > 62:
		return "wPrawo"

######################KALIBRACJA

bialy1 = 0
czarny1 = 0
sredni1 = 0
bialy2 = 0
czarny2 = 0
sredni2 = 0

kal_bial1 = 0
kal_czarn1 = 0
kal_bial2 = 0
kal_czarn2 = 0

sound.speak("WHITE", True)
print("KOLOR BIALY")
while not start.value(): time.sleep(0.1)

while kal_bial1 < 10:
	kal_bial1 += 1
	bialy1 += kolor.value()
	time.sleep(0.1)
bialy1 = bialy1/10

while kal_bial2 < 10:
	kal_bial2 += 1
	bialy2 += swiatlo.value()
	time.sleep(0.1)
bialy2 = bialy2/10
sound.speak("OK", True)

sound.speak("BLACK", True)
print("KOLOR CZARNY")

while not start.value(): time.sleep(0.1)
while kal_czarn1 < 10:
	kal_czarn1 += 1
	czarny1 += kolor.value()
	time.sleep(0.1)
czarny1 = czarny1/10

while kal_czarn2 < 10:
	kal_czarn2 += 1
	czarny2 += kolor.value()
	time.sleep(0.1)
czarny2 = czarny2/10
sound.speak("OK", True)

sredni1 = 0.5*(bialy1+czarny1)
sredni2 = 0.5*(bialy2+czarny2)
sound.speak("END", True)
sleep(1)

####################PROGRAM

while (start.value() != 1):
	time.sleep(0.1)

'''Kp1 = 1.65 
Ki1 = 3.6 #2.5
Kd1 = 0.1'''
Td1 = 0.01
Ti1 = 0.01
'''Kp2 = 0.4 
Ki2 = 1.3 #0.1
Kd2 = 0.01''' 
Td2 = 0.01
Ti2 = 0.01
i_aw = 200
#wz_p = 300
mmax = 500
mmin = 0
#dobre nastawy, ale wolne
Kp1 = 1.6
Ki1 = 1.35 
Kd1 = 0.015
Kp2 = 0.4 
Ki2 = 0.065 
Kd2 = 0.01 #0.01 
wz_p = 150
ostatniSkret = "L"

PID1 = 0
P1 = 0
I1 = 0
D1 = 0
PID2 = 0
P2 = 0
I2 = 0
D2 = 0

i_pamiec = 5

error1 = 0
last_error1 = 0
i_error1 = 0
ls_error1 = [0]*i_pamiec
error2 = 0
last_error2 = 0
i_error2 = 0
ls_error2 = [0]*i_pamiec

lmotor_ster = 0
rmotor_ster = 0

odczyt1 = 0
odczyt2 = 0

czyWjechalNaZakretZPrawej = False
czyWjechalNaZakretZLewej = False
t1 = t2 = t3 = 0.0
czy1 = False
czy2 = False
pamiecSkretow = "L"
while True:
	odczyt1 = kolor.value()
	odczyt2 = swiatlo.value()
	
	#######################PRZESZKODA

	if czyPrzeszkoda() == True:
		ominPrzeszkode("lewo")
	
	#REGULATOR NR 1

	last_error1 = error1
	error1 = bialy1 - odczyt1

	for k in range(0,i_pamiec):
		if (k < i_pamiec - 1):
			ls_error1[k] = ls_error1[k+1]
		else:
			ls_error1[k] = error1

	#blad czlonu calkujacego

	i_error1 = 0

	for l in range(0,i_pamiec):
		i_error1 += ls_error1[l]

	if i_error1 > i_aw:
		i_error1 = i_aw
	elif i_error1 < -i_aw:
		i_error1 = -i_aw

	P1 = Kp1 * error1
	I1 = Ki1 * i_error1 * Ti1 * i_pamiec
	D1 = Kd1 * (last_error1 - error1) / Td1

	PID1 = (P1 + I1 + D1)

	#REGULATOR NR 2

	last_error2 = error2
	error2 = bialy2 - odczyt2

	for k in range(0,i_pamiec):
		if (k < i_pamiec - 1):
			ls_error2[k] = ls_error2[k+1]
		else:
			ls_error2[k] = error2

	#blad czlonu calkujacego

	i_error2 = 0

	for l in range(0,i_pamiec):
		i_error2 += ls_error2[l]

	if i_error2 > i_aw:
		i_error2 = i_aw
	elif i_error2 < -i_aw:
		i_error2 = -i_aw
	
	P2 = Kp2 * error2
	I2 = Ki2 * i_error2 * Ti2 * i_pamiec
	D2 = Kd2 * (last_error2 - error2) / Td2

	PID2 = (P2 + I2 + D2)

	lmotor_ster = wz_p - PID1 + PID2
	rmotor_ster = wz_p + PID1 - PID2

	if lmotor_ster > mmax:
		lmotor_ster = mmax
	elif lmotor_ster < mmin:
		lmotor_ster = mmin

	if rmotor_ster > mmax:
		rmotor_ster = mmax
	elif rmotor_ster < mmin:
		rmotor_ster = mmin

	if odczyt1 > sredni1 + 10 and odczyt2 > 340:
		rmotor_ster = lmotor_ster

	'''# wjezdza z prawej na kat prosty

	if odczyt1 < sredni1 - 10 and odczyt2 > 340:
		czyWjechalNaZakretZPrawej = True

	if czyWjechalNaZakretZPrawej == True:
		t1 = time.time()
		czyWjechalNaZakretZPrawej = False
		czy1 = True
	

	if odczyt1 > sredni1 + 10 and odczyt2 > 340 and czy1 == True:
		t2 = time.time()
		czy1 = False
		t3 = t2 - t1
		if t3 > 0.033 and t3 < 0.046: #.031   0.2599 0.2294 0.0419 0.0451 0.0483
			obrocRobota(-70)

	# wjezdza z lewej  na kat prosty

	if odczyt1 > sredni1 + 10 and odczyt2 < 340:
		czyWjechalNaZakretZLewej = True

	if czyWjechalNaZakretZLewej == True:
		t1 = time.time()
		czyWjechalNaZakretZLewej = False
		czy2 = True
	

	if odczyt1 > sredni1 + 10 and odczyt2 > 340 and czy2 == True:
		t2 = time.time()
		czy2 = False
		t3 = t2 - t1
		if t3 > 0.02 and t3 < 0.026:  # 0.02407 0.2261 0.02595 0.2036 zle 0.02298  0.0209 0.02339 0.02165
			obrocRobota(110)'''

		#MaxBlack1 = 48
	#MaxBlack2 = 480
	# bialy 1 = 10 2 = 300 czarny 1 = 55 2 = 550 
	#if odczyt1 > lMaxBlack and odczyt2 > rMaxBlack:
	 
	ostatniSkret = "prosto"
	if odczyt1 > odczyt2 / 10 and odczyt1 > 45 and odczyt2 < 360 and odczyt2 > 300: # odczyt2 < 360  
		ostatniSkret = "L"
		pamiecSkretow = "L"
	elif odczyt2 > odczyt1 and odczyt2 < 115 and odczyt1 < 75 : #110 75
		ostatniSkret = "R"
		pamiecSkretow = "R"

	if ostatniSkret == "prosto" and odczyt1 < 15 and  odczyt2 < 400 and pamiecSkretow == "L":
		while kolor.value() < 30 and not start.value() :
			lmotor.run_forever(speed_sp = -150)
			rmotor.run_forever(speed_sp = 310) 
		pamiecSkretow = "prosto"

	if ostatniSkret == "prosto" and odczyt1 < 70  and  odczyt2 < 400 and pamiecSkretow == "R":
		while swiatlo.value() < 350 and not start.value() :
			lmotor.run_forever(speed_sp = 350)
			rmotor.run_forever(speed_sp = -100)
		pamiecSkretow = "prosto"


	#if ostatniSkret == "R" and odczyt1 > odczyt2 / 10:
	#	lmotor.run_forever(speed_sp = 400)
	#	rmotor.run_forever(speed_sp = -20)
	#if ostatniSkret == "L" and 	
	

	lmotor.run_forever(speed_sp = int(lmotor_ster))
	rmotor.run_forever(speed_sp = int(rmotor_ster))

	#print("###############################")
	#print(lmotor_ster, "lm")
	#print(rmotor_ster, "lr")
	#print(t3, "T")
	print(odczyt1)
	print(odczyt2)
	print str(ostatniSkret)
	
	

	if start.value() == 1:
		time.sleep(1)
		if start.value() == 1:
			break

	sleep(0.005)
